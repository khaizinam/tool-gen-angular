
import os
from readCSV import *
from Utils import *
# csv read
csvReader = CSVReader('table.json')
print('read file success')

class Angular:
    def __init__(self, csvReader:CSVReader):
        self.rowsTable:RowObj
        self.id = csvReader.id
        self._html = '.component.html'
        self._ts = '.component.ts'
        self._model = '.model.ts'
        self.csvReader = csvReader
        self.childs:Element = []
        
    def writeToFile(self,path, msg):
        os.makedirs(os.path.dirname("exportFile/" + path), exist_ok=True)
        f = open("exportFile/" + path, "w", encoding="utf-8")
        f.write(msg)
        f.close()
        f = open("exportFile/" + path, "r")
        
    def createDisplayedHeaderColSpan(self):
        ctx = "  displayedHeaderColSpan: string[] = [\n"
        colsList = self.csvReader.displayedHeaderColSpan
        for colNM in colsList:
            ctx += "    '{}',\n".format(colNM)
        ctx = ctx[:-2]
        ctx += "\n  ];\n".format(colNM)
        return ctx
    
    def createDisplayedSubHeader(self):
        ctx = "  displayedSubHeader: string[] = [\n"
        colsList = self.csvReader.displayedSubHeader
        for colNM in colsList:
            ctx += "    '{}',\n".format(colNM)
        ctx = ctx[:-2]
        ctx += "\n  ];\n".format(colNM)
        return ctx
    
    def createDisplayedColumns(self):
        ctx = "  displayedColumns: string[] = [\n"
        colsList = self.csvReader.displayedColumns
        for colNM in colsList:
            ctx += "    '{}',\n".format(colNM)
        ctx = ctx[:-2]
        ctx += "\n  ];\n".format(colNM)
        return ctx
    
    def createDisplayCol(self):
        ctx = "  /* Create displayed Constant */\n"
        ctx += self.createDisplayedHeaderColSpan()
        ctx += self.createDisplayedSubHeader()
        ctx += self.createDisplayedColumns()
        return ctx
    
    def createcolSpan(self):
        ctx = "  colSpans = {"
        for col in self.csvReader.colSpan:
            ctx += ' {}: {},'.format(col[0],col[1])
        ctx = ctx[:-1]
        ctx += ' };\n'
        return ctx
    
    def resizeTable(self):
        ctx = "\n  private resizeTable() {\n    const columnResizeTmp: ResizeColumnModel[] = [\n"
        for rowNM in self.csvReader.displayedColumns: 
            ctx +=  """      { columnName: '""" + rowNM + """', width: 100 },\n"""
        ctx = ctx[:-2]
        ctx +="\n    ];\n"
        ctx +="    const columnResize: ResizeColumnModel[] = [];\n"
        ctx +="    for (const i of columnResizeTmp) {\n"
        ctx +="      if (!this.columnDisplay[i.columnName].hidden) {\n"
        ctx +="        columnResize.push({ columnName: i.columnName, width: i.width });\n"
        ctx +="      }\n    }\n"
        ctx +="    this.initTable('" +self.csvReader.id + "-tb', columnResize);\n  }\n"
        return ctx
    
    def createConstItem(self):
        ctx = "\n/* item screen */\n"
        ctx += "  ITEM_SCREEN = {\n"
        for row in self.csvReader.rows:
            ctx += "    "+ row.name +" : { columnName : '" + row.name + "', JP : '"+ row.jp +"' },\n"
        ctx += "  };\n\n"
        
        for row in self.csvReader.rows:
            if row.type == TYPE_COMBO:
                ctx += "/* Item {} */\n".format(row.name.upper())
                ctx += "  " + row.name.upper() + '_OPTION = [\n'
                for option in row.dataSelect:
                    ctx += "    { value : " + str(option[0]) + ", label : '" + option[1] + "' },\n"
                ctx += "  ]\n\n"
        return ctx
    def createConstructor(self):
        id = self.csvReader.id.upper()
        ctx  = "\n"
        ctx  += '  constructor(\n'
        ctx  += '    private permissionService: PermissionService,\n'
        ctx  += '    private matDialog: MatDialog,\n'
        ctx  += '    private router: Router,\n'
        ctx  += '    private {}Service: {}Service\n'.format(self.csvReader.id, id)
        ctx  += '  ) {\n'
        ctx  += '    super();\n'
        ctx  += '    this.columnDisplay = {\n'
        ctx  += '    ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])\n'
        ctx  += '    };\n'
        ctx  += '    this.columnDisplayTmp = {\n'
        ctx  += '    ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])\n'
        ctx  += '    };\n'
        ctx  += '  };\n'
        return ctx
    
    def createOnInit(self):
        ctx = "\n"
        ctx += '  ngOnInit(): void {\n'
        ctx += '    this.processInit();\n'
        ctx += '  };\n'
        return ctx
    
    def creategAfterViewInit(self):
        ctx = "\n"
        ctx += '  ngAfterViewInit() {\n'
        ctx += '    this.resizeTable();\n'
        ctx += '    this.permissionService.initPermission(this.screen.functionId);\n'
        ctx += '  }\n'
        return ctx
    
    def processInit(self):
        ctx = "\n  processInit() {\n"
        ctx += '    this.searchModel.pageNumber = 1;\n'
        ctx += '    this.searchModel.sortConditions = []\n'
        ctx += '  }\n'
        return ctx
    def importPackage(self):
        id = self.csvReader.id.upper()
        ctx = ""
        ctx += "import { AfterViewInit, Component, OnInit } from '@angular/core';\n"
        ctx += "import { MatDialog } from '@angular/material/dialog';\n"
        ctx += "import { MatRadioChange } from '@angular/material/radio';\n"
        ctx += "import { MatTableDataSource } from '@angular/material/table';\n"
        ctx += "import { Router } from '@angular/router';\n\n"
        ctx += "import {\n"
        ctx += "  ACTION_TYPE_CONST,\n"
        ctx += "  BUTTON,\n"
        ctx += "  CONFIRM_DIALOG_CONTS,\n"
        ctx += "  EXPORT_CONST,\n"
        ctx += "  FORMAT_DATE_CONST,\n"
        ctx += "  HTTP_STATUS,\n"
        ctx += "  PAGINATION_CONST,\n"
        ctx += "  PATTERN_CONST,\n"
        ctx += "  ROLE_CONST,\n"
        ctx += "  SCREEN_CONST,\n"
        ctx += "  SORT_TYPE,\n"
        ctx += "} from 'src/app/shared/constants/common.constant';\n\n"
        ctx += "import { MESSAGES_CODE } from 'src/app/shared/constants/messages.constant';\n"
        ctx += "import { PERMISSION_ACTION_CONST } from 'src/app/shared/constants/permission.constant';\n"
        ctx += "import { TableHandle } from 'src/app/shared/handles/table-v1.handle';\n"
        ctx += "import { ColumnDataModel } from 'src/app/shared/models/display-item/column-data.model';\n"
        ctx += "import { DisplayItemConfigModel } from 'src/app/shared/models/display-item/display-item-config.model';\n"
        ctx += "import { SortDataModel } from 'src/app/shared/models/display-item/sort-data.model';\n"
        ctx += "import { ExportModel } from 'src/app/shared/models/export.model';\n"
        ctx += "import { ResizeColumnModel } from 'src/app/shared/models/resize-column.model';\n"
        ctx += "import { AuthService } from 'src/app/shared/services/auth.service';\n"
        ctx += "import { PermissionService } from 'src/app/shared/services/permission.service';\n"
        ctx += "import { "+ id +"Service } from 'src/app/shared/services/" + self.csvReader.id + ".service';\n"
        ctx += "import { CommonUtil } from 'src/app/shared/utils/common.util';\n"
        ctx += "import { DialogUtil } from 'src/app/shared/utils/dialog.util';\n"
        ctx += "import { getErrorMessageResponse, getMessage } from 'src/app/shared/utils/message.util';\n"
        ctx += "import { convertTextCsv, formatString } from 'src/app/shared/utils/string.util';\n\n"
        return ctx
        
    def exportComponent(self):
        id = self.csvReader.id.upper()
        ctx = ""
        ctx += self.importPackage()
        ctx += """@Component({ selector: 'app-"""+id+"""',templateUrl: './"""+id+""".component.html',styleUrls: ['./"""+id+""".component.scss']})\n"""
        ctx += """export class """+id+"""Component\n  extends TableHandle\n  implements OnInit, AfterViewInit\n{\n\n"""
        ctx += '  screen = SCREEN_CONST.' + id + ';\n'
        ctx += '  button = BUTTON;\n'
        ctx += '  isEdit = false;\n'
        ctx += '  permissionActionConst = PERMISSION_ACTION_CONST;\n'
        ctx += "  message = '';\n"
        ctx += '  panelOpenSearch = true;\n'
        ctx += '  panelOpenDisplayItem = true;\n'
        ctx += '  panelOpenResult = false;\n'
        ctx += '  isAddNewMode = false;\n\n'
        ctx += '  columnDisplay = {};\n'
        ctx += '  columnDisplayTmp = {};\n'
        ctx += '  sortCols: SortDataModel[] = [];\n'
        ctx += "  fixedCols = ['selection'];\n"
        ctx += '  searchModel = new ' + id + 'SearchModel();\n'
        ctx += '  tableErrorModel = new {}tableErrorModel();\n'.format(id)
        ctx += '  editModel = new {}TableModel();\n'.format(id)
        ctx += '  dataSource: MatTableDataSource<{}TableModel> = new MatTableDataSource<{}TableModel>();\n'.format(id,id)
        ctx += self.createConstItem()
        ctx += self.createcolSpan()
        ctx += self.createDisplayCol()
        ctx += self.createConstructor()
        ctx += self.createOnInit()
        ctx += self.creategAfterViewInit()
        ctx += self.resizeTable()
        ctx += self.processInit()
        ctx +='}'
        self.writeToFile(f'{self.id}/{self.id}{self._ts}',ctx)
        
    def initDom(self):
        self.initTable(0)
        div = Div(Util(self.csvReader.id, 0,self.csvReader,self.csvReader.rows[0]),[],self.childs)
        div.params = ['class="{}"'.format(self.csvReader.id)]
        self.childs = [div]
    
    def exportHTML(self):
        print(f'-> Start export {self.id}{self._html}')
        context = ""
        for element in self.childs:
            context+= element.getCTX()
        self.writeToFile(f'{self.id}/{self.id}{self._html}',context)
        
    def createUpdateModel(self):
        id = self.csvReader.id.upper()
        ctx = "/* Update Model */\n"
        ctx += "export class "+id+"UpdateModel {\n"
        for row in self.csvReader.rows:
            if row.isEdit:
                if row.type == TYPE_TEXT:
                    ctx += "  {}: string;\n".format(row.name)
                elif row.type == TYPE_COMBO:
                    ctx += "  {}: number;\n".format(row.name)
        ctx += "}\n"
        return ctx
    
    def createInsertModel(self):
        id = self.csvReader.id.upper()
        ctx = "/* Insert Model */\n"
        ctx += "export class "+id+"InsertModel {\n"
        for row in self.csvReader.rows:
            if row.isAdd:
                if row.type == TYPE_TEXT:
                    ctx += "  {}: string;\n".format(row.name)
                elif row.type == TYPE_COMBO:
                    ctx += "  {}: number;\n".format(row.name)
        ctx += "}\n"
        return ctx
    
    def createErrortModel(self):
        id = self.csvReader.id.upper()
        ctx = "/* Error Model */\n"
        ctx += "export class "+id+"tableErrorModel {\n"
        for row in self.csvReader.rows:
            if row.isAdd or row.isEdit:
                ctx += "  {}: ErrorControlModel = new ErrorControlModel();\n".format(row.name)
        ctx += "}\n"
        return ctx
    
    def createTableModel(self):
        id = self.csvReader.id.upper()
        ctx = "/* Table Model */\n"
        ctx += "export class "+id+"TableModel {\n"
        for row in self.csvReader.rows:
            if row.type == TYPE_TEXT:
                ctx += "  {}: string;\n".format(row.name)
            elif row.type == TYPE_COMBO:
                ctx += "  {}: number;\n".format(row.name)
        ctx += "  edited: boolean;\n"
        
        ctx += "  constructor() {\n"
        for row in self.csvReader.rows:
            if row.type == TYPE_TEXT:
                ctx += "    this.{} = '';\n".format(row.name)
            elif row.type == TYPE_COMBO:
                ctx += "    this.{} = 0;\n".format(row.name)
        ctx += "    this.edited = false;\n"
        ctx += "  }\n"
        ctx += "}\n"
        return ctx
     
    def exportModel(self):
        context = "import { CommonRequestModel } from '../common-request.model';\n"
        context +="import { ErrorControlModel } from '../error-control.model';\n"
        context += "\n/* Model */\n\n"
        context += self.createTableModel()
        context += self.createUpdateModel()
        context += self.createInsertModel()
        context += self.createErrortModel()
        self.writeToFile(f'{self.id}/{self.id}{self._model}',context)
    
    def export(self):
        self.exportHTML()
        print('-> Start export file component')
        self.exportComponent()
        print('-> Start export file Model')
        self.exportModel()
        print('Export files complete !!!')
        
    def showModeRef(self, o:Util):
        if o.row.type == TYPE_TEXT:
            span = Span(o, ['*ngIf="!element.edited"'],[])
            span.content = "{{" +'element.{}'.format(o.row.name) +"}}"
            return [span]
        elif o.row.type == TYPE_COMBO:
            div = Div(o, ['*ngIf="!element.edited"','[ngSwitch]="element.{}"'.format(o.row.name)],[])
            childs = []
            newo = Util(o.name, o.lvl + 1,self.csvReader,o.row)
            for case in o.row.dataSelect:
                span = Span(newo, ['*ngSwitchCase="{}"'.format(case[0])],[])
                span.content = case[1]
                childs+=[span]
            div.childs = childs
            return [div]
        
    def showModeEdit(self, o:Util):
        if o.row.type == TYPE_COMBO:
            return self.createSelectGroup(o,'element.edited')
        elif o.row.type == TYPE_TEXT:
            return self.createInputGroup(o,'element.edited')
        
    def createSelectGroup(self, o:Util,cond:str):
        newo = Util(o.name,o.lvl + 1,self.csvReader , o.row)
        matSelect = MatSelect(newo.lvl+1,o.row.name,[],o.row.dataSelect)
        form = MatFormFiled(newo,[],[matSelect])
        div = Div(o,
                    ['class="cm-form-control"','*ngIf="{}"'.format(cond)],
                    [form])
        divError = Div(o,
                    ['class="cm-message-error"','*ngIf="{} && tableErrorModel.{}.isError"'.format(cond, o.row.name)],
                    [Text( getSpace(o.lvl + 1) + '{{'+ 'tableErrorModel.{}.msg'.format(o.row.name)+ '}}\n' )])
        return [div,divError]
    
    def createInputGroup(self,o:Util,cond:str):
        input = Input( o.lvl + 1,
                    o.row.name,
                    'editModel.{}'.format(o.row.name), 
                    [])
        buttonDelete = ButtonDelete( o.lvl + 1,o.row.name,'editModel',[])
        div = Div(o,
                ['class="cm-form-control"',
                '*ngIf="{}"'.format(cond)],
                [input, buttonDelete])
        divError = Div(o,
                    ['class="cm-message-error"','*ngIf="{} && tableErrorModel.{}.isError"'.format(cond,o.row.name)],
                    [Text( getSpace(o.lvl + 1) + '{{'+ 'tableErrorModel.{}.msg'.format(o.row.name)+ '}}\n' )])
        return [div,divError]
    
    def visitBody(self, o:Util):
        body = TableBody(o, [], [])
        if o.row.type == TYPE_SELECT:
            body.childs += [MatRadioButton(o.lvl + 1)]
        else:
            if o.row.isEdit:
                # show mode ref
                newo = Util(o.name, o.lvl + 1,self.csvReader,o.row)
                body.childs += self.showModeRef(newo)
                body.childs += self.showModeEdit(newo)
            else:
                newo = Util(o.name, o.lvl + 1,self.csvReader,o.row)
                span = Span(newo, [],[])
                span.content = "{{" +'element.{}'.format(o.row.name) +"}}"
                body.childs += [span]
        return [body]
    
    def visitFooter(self,o:Util):
        footer = TableFooter(o, [], [])
        newo = Util(o.name, o.lvl +1,self.csvReader, o.row)
        if o.row.type == TYPE_SELECT:
            span = Span( newo,['*ngIf="!isAddNewMode && !isEdit"','(click)="handleAddRow()"','class="cm-cursor-pointer"'],[])
            span.content = "{{ button.add_row }}"
            footer.childs += [span]
        elif o.row.isAdd:
            if o.row.type == TYPE_COMBO:
                footer.childs +=self.createSelectGroup(o,'isAddNewMode')
            elif o.row.type == TYPE_TEXT:
                footer.childs += self.createInputGroup(o,'isAddNewMode')
        return [footer]
    
    def initTable(self,lvl):
        childs: Element = []
        for row in self.rowsTable:
            util = Util(row.name, lvl+7, csvReader, row)
            util2 = Util(row.name, lvl+8, csvReader, row)
            th = TableHead(util2, [], [])
            ngContainerChilds = [th] 
            if row.type != "label":
                util2.lvl = lvl+8
                ngContainerChilds += self.visitBody(util2)
                if self.csvReader.footer != False:
                    ngContainerChilds += self.visitFooter(util2)
                childs += [NgContainer(util, [], ngContainerChilds)]
            else:
                childs += [NgContainer(util, [], [th])]
        matTable = MatTable(Util('s0901', lvl + 6, csvReader, csvReader.rows[0]), [], childs)
        o = Util('s0901', lvl, csvReader, csvReader.rows[0])
        
        divTable1 = Div(o,[],[matTable])
        divTable1.lvl = lvl + 5
        divTable1.params = ['class="table-reponsive"']
        
        divTable = Div(o,[],[divTable1])
        divTable.lvl = lvl + 4
        divTable.params = ['class="cm-expansion-panel-content"']
        
        element3 = Element(o,[], [Text(getSpace(lvl + 5) + '<mat-panel-title> 【検索結果一覧】 </mat-panel-title>\n')])
        element3.nameTag = 'mat-expansion-panel-header'
        element3.lvl = lvl + 4
        # <mat-expansion-panel-header>
        #     <mat-panel-title> 【検索結果一覧】 </mat-panel-title>
        # </mat-expansion-panel-header>
        # parent 3
        element = Element(o,
                        ['class="header-search"',
                        '(opened)="panelOpenResult = true"',
                        '[expanded]="panelOpenResult"',
                        '(closed)="panelOpenResult = false"'], [ element3, divTable])
        element.nameTag = 'mat-expansion-panel'
        element.lvl = lvl + 3
        
        # parent 2
        element1 = Element(o,
                        [],[element])
        element1.nameTag = 'mat-accordion'
        element1.lvl = lvl + 2
        
        # parent 1
        element2 = Div(o, ['class="cm-expansion-panel"'],[element1])
        element2.lvl = lvl + 1
        
        self.childs += [element2]
        
angular = Angular(csvReader)
angular.rowsTable = csvReader.getRow()
angular.initDom()
angular.export()





