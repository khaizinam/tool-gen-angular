import json
from constant import *

class RowObj:
    def __init__(self, name: str, jp: str, type: str, isEdit: bool, isAdd: bool, dataSelect: str):
        self.name = name
        self.jp = jp
        self.type = type  # text,combo,select
        self.isEdit = isEdit
        self.isAdd = isAdd
        self.colSpan = 1
        self.rowSpan = 2
        self.dataSelect = self.combine(dataSelect)
        self.subHeader=[]

    def combine(self, dataSelect):
        if self.type != 'combo':
            return []
        arr2 = []
        for i in dataSelect:
            arr2.append(i)
        return arr2

    def print(self):
        print(
            f'{self.name},{self.jp},{self.type},{self.isEdit},{self.isAdd},{self.dataSelect},')


class CSVReader:
    def __init__(self, f_path: str):
        self.id: str = ''
        self.rows: RowObj = []
        self.footer: bool = False
        self.displayedHeaderColSpan = []
        self.displayedSubHeader = []
        self.displayedColumns = []
        self.colSpan = []
        self.read(f_path)
    def inputRow(self, row):
        if row['type'] == TYPE_COMBO:
            return RowObj(row['name'], row['jp'], row['type'],
                                row['edit'], row['add'], row['data'])
        elif row['type'] == TYPE_SELECT:
                return RowObj(row['name'], row['jp'], row['type'],
                                False, row['add'], [])
        else:
            return RowObj(row['name'], row['jp'], row['type'],
                            row['edit'], row['add'], [])
        
    def inputSubRow(self,row):
        newRow = RowObj(row['name'], row['jp'], row['type'],
                        '', '', [])
        subRows = row['columns']
        for subRow in subRows:
            newRow.subHeader.append(subRow['name'])
        newRow.colSpan = len(subRows)
        newRow.rowSpan = 1
        newRows = [newRow]
        for subRow in subRows:
            self.displayedSubHeader.append(subRow['name'])
            self.displayedColumns.append(subRow['name'])
            if subRow['type'] == TYPE_COMBO:
                newRow = RowObj(subRow['name'], subRow['jp'], subRow['type'],
                                subRow['edit'], subRow['add'], subRow['data'])
            
            elif subRow['type'] == TYPE_SELECT:
                newRow = RowObj(subRow['name'], subRow['jp'], subRow['type'],
                                False, subRow['add'], [])
            else:
                newRow = RowObj(subRow['name'], subRow['jp'], subRow['type'],
                                subRow['edit'], subRow['add'], [])
            newRow.rowSpan = 1
            newRows += [newRow]
        return newRows
    def read(self, f_path):
        f = open(f_path, mode='r', encoding='utf8')
        data = json.load(f)
        rowObj = []
        self.id = data['id']
        self.footer = data['footer']
        for row in data["table"]:
            self.displayedHeaderColSpan.append(row['name'])
            if row['type'] != 'label':
                self.displayedColumns.append(row['name'])
                rowObj += [self.inputRow(row)]
            else:
                self.colSpan.append([row['name'],len(row['columns'])])
                rowObj += self.inputSubRow(row)
        self.addRow(rowObj)

    def addRow(self, rows):
        self.rows = rows

    def getRow(self):
        return self.rows

    def print(self):
        for rowObj in self.rows:
            rowObj.print()
