import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { S3301Component } from './s3301.component';

const routes: Routes = [{ path: '', component: S3301Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class S3301RoutingModule {}
