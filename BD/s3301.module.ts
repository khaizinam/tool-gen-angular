import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { S3301RoutingModule } from './s3301-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { S3301Component } from './s3301.component';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { DisplayConditionModule } from 'src/app/shared/components/display-condition/display-condition.module';
import { ActionBarModule } from 'src/app/shared/components/action-bar/action-bar.module';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { DialogConfirmModule } from 'src/app/shared/components/dialog-confirm/dialog-confirm.module';
import { S3301Service } from 'src/app/shared/services/s3301.service';

@NgModule({
  declarations: [S3301Component],
  imports: [
    CommonModule,
    FormsModule,
    S3301RoutingModule,
    ReactiveFormsModule,

    MatFormFieldModule,
    MatExpansionModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatDialogModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    DisplayConditionModule,
    ActionBarModule,
    DialogConfirmModule,
    MatButtonModule
  ],
  providers: [S3301Service]
})
export class S3301Module {}
