import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { ExportDataService } from './export-data.service';
import { API_CONST } from '../constants/common.constant';
import * as s3301Model from '../models/s3301/s3301.model';

@Injectable()
export class S3301Service extends BaseService {
  public constructor(
    private httpClient: HttpClient,
    private exportDataService: ExportDataService
  ) {
    super(httpClient, exportDataService, API_CONST.S3301.CONTROLLER_NAME);
  }

  initDataSearch() {
    return this.get(API_CONST.S3301.METHODS.INIT);
  }
  onSearchData(payload: s3301Model.S3301SearchModel) {
    return this.post(API_CONST.S3301.METHODS.SEARCH, payload);
  }
  onUpdateMMC(payload: s3301Model.S3301UpdateModel) {
    return this.put(API_CONST.S3301.METHODS.UPDATE, payload);
  }
  onInsertMMC(payload: s3301Model.S3301InsertModel) {
    return this.post(API_CONST.S3301.METHODS.CREATE, payload);
  }
  onDeleteMMC(payload: any) {
    return this.delete(API_CONST.S3301.METHODS.DELETE, payload);
  }
  exportDataToCSV(payload: s3301Model.S3301SearchModel) {
    return this.post(API_CONST.S3301.METHODS.EXPORT_CSV, payload);
  }
  onValidate(payload: any) {
    return this.get(API_CONST.S3301.METHODS.VALIDATE, payload);
  }
}
