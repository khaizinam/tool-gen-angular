import { CommonRequestModel } from '../common-request.model';
import { ErrorControlModel } from '../error-control.model';

export class S3301SearchModel extends CommonRequestModel {
  availability: number = null;
  userID = '';
  organizationID = '';
  level: number = null;
}
export class S3301Organization {
  organizationID = '';
  organizationName = '';
  factory = '';
}
export class S3301InitModel {
  userIDs: string[] = [];
  organizations: S3301Organization[] = [];
}
export class S3301TableModel {
  availability: number;
  userID: string;
  name: string;
  organizationID: string;
  organizationNM: string;
  factory: string;
  post: number;
  caseFormApprovalStatus: number;
  caseFormManager: number;
  editLevel: number;
  comment: string;
  lastLoginDT: string;
  updateDT: string;
  mailSendDivision: number;
  updateUserID: string;
  edited: boolean;
  constructor() {
    this.userID = '';
    this.availability = 1;
    this.name = '';
    this.organizationID = '';
    this.organizationNM = '';
    this.factory = '';
    this.post = 0;
    this.caseFormApprovalStatus = 0;
    this.caseFormManager = 0;
    this.editLevel = 0;
    this.comment = '';
    this.lastLoginDT = '';
    this.updateDT = '';
    this.mailSendDivision = 0;
    this.updateUserID = '';
    this.edited = false;
  }
}

export class S3301UpdateModel {
  userID: string;
  name: string;
  organizationID: string;
  availability: number;
  post: number;
  caseFormApprovalStatus: number;
  caseFormManager: number;
  editLevel: number;
  comment: string;
  lastLoginDT: string;
  mailSendDivision: number;
  updateDT: string;
  selfUserID: string;
  selfOrganizationID: string;
}

export class S3301InsertModel {
  userID: string;
  name: string;
  organizationID: string;
  availability: number;
  post: number;
  caseFormApprovalStatus: number;
  caseFormManager: number;
  editLevel: number;
  comment: string;
  lastLoginDT: string;
  mailSendDivision: number;
  updateDT: string;
  selfUserID: string;
  selfOrganizationID: string;
}

export class S3301DeleteModel {
  userID: string;
  organizationID: string;
}
export class S3301CSVModel extends CommonRequestModel {
  availability: number = null;
  userID = '';
  organizationID = '';
}

export class S3301SearchErrorModel {
  userID: ErrorControlModel = new ErrorControlModel();
}

export class S3301tableErrorModel {
  userID: ErrorControlModel = new ErrorControlModel();
  name: ErrorControlModel = new ErrorControlModel();
  organizationID: ErrorControlModel = new ErrorControlModel();
  post: ErrorControlModel = new ErrorControlModel();
  caseFormApprovalStatus: ErrorControlModel = new ErrorControlModel();
  caseFormManager: ErrorControlModel = new ErrorControlModel();
  editLevel: ErrorControlModel = new ErrorControlModel();
  comment: ErrorControlModel = new ErrorControlModel();
}
