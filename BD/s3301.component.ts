import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { catchError } from 'rxjs';

import {
  ACTION_TYPE_CONST,
  BUTTON,
  CHAR_ITEM,
  CONFIRM_DIALOG_CONTS,
  EXPORT_CONST,
  FORMAT_DATE_CONST,
  HTTP_STATUS,
  PAGINATION_CONST,
  PATTERN_CONST,
  ROLE_CONST,
  SCREEN_CONST,
  SORT_TYPE,
  s3301_AVAILABLE_CONST,
  s3301_CONST_ITEM
} from 'src/app/shared/constants/common.constant';
import { MESSAGES_CODE } from 'src/app/shared/constants/messages.constant';
import { PERMISSION_ACTION_CONST } from 'src/app/shared/constants/permission.constant';
import { S3301OPTION_ITEM } from 'src/app/shared/constants/screen-item.constant';
import { TableHandle } from 'src/app/shared/handles/table-v1.handle';
import { ColumnDataModel } from 'src/app/shared/models/display-item/column-data.model';
import { DisplayItemConfigModel } from 'src/app/shared/models/display-item/display-item-config.model';
import { SortDataModel } from 'src/app/shared/models/display-item/sort-data.model';
import { ExportModel } from 'src/app/shared/models/export.model';
import { ResizeColumnModel } from 'src/app/shared/models/resize-column.model';
import {
  S3301CSVModel,
  S3301InitModel,
  S3301InsertModel,
  S3301SearchErrorModel,
  S3301SearchModel,
  S3301TableModel,
  S3301UpdateModel,
  S3301tableErrorModel
} from 'src/app/shared/models/s3301/s3301.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { S3301Service } from 'src/app/shared/services/s3301.service';
import { CommonUtil } from 'src/app/shared/utils/common.util';
import { DialogUtil } from 'src/app/shared/utils/dialog.util';
import {
  getErrorMessageResponse,
  getMessage
} from 'src/app/shared/utils/message.util';
import { convertTextCsv, formatString } from 'src/app/shared/utils/string.util';
@Component({
  selector: 'app-s3301',
  templateUrl: './s3301.component.html',
  styleUrls: ['./s3301.component.scss']
})
export class S3301Component
  extends TableHandle
  implements OnInit, AfterViewInit
{
  /*---------------------------------------*/
  /*        Status button variable         */
  /*---------------------------------------*/
  button = BUTTON;
  isEdit = false;
  isAddNewMode = false;
  isActiveCSV = false;
  constItem = S3301OPTION_ITEM;
  colSpans = { organization: 3, packageAuthority: 2, finalUpdate: 2 };

  /*---------------------------------------*/
  /*                VARIABLE               */
  /*---------------------------------------*/
  // const
  formatDt = FORMAT_DATE_CONST.yyyyMMddHHmm;
  TRANS = s3301_CONST_ITEM;
  screen = SCREEN_CONST.S3301;
  availability = s3301_AVAILABLE_CONST;
  permissionActionConst = PERMISSION_ACTION_CONST;
  // variable
  message = `MMCユーザーを管理します。`;
  panelOpenSearch = true;
  panelOpenDisplayItem = true;
  panelOpenResult = false;
  pageSizeTmp: number;
  userLogged;

  isSelectedRow = false;
  columnDisplay = {};
  columnDisplayTmp = {};
  fixedCols = ['selection'];
  // model
  searchModel = new S3301SearchModel();
  initModel = new S3301InitModel();
  displayCols: ColumnDataModel[] = [];
  sortCols: SortDataModel[] = [];
  currentPage = 1;
  dataSource: MatTableDataSource<S3301TableModel> =new MatTableDataSource<S3301TableModel>();
  s3301Model: S3301TableModel = new S3301TableModel();
  oldUserID: string;
  organizationID: string;
  tableErrorModel = new S3301tableErrorModel();
  searchErrorModel = new S3301SearchErrorModel();

  /*--------------------------------*/
  /*        INIT TABLE COLs         */
  /*--------------------------------*/
  nameJPItemSearch = {
    availability: '使用有無',
    UserID: 'ユーザーID',
    organizationNM: '組織名称',
    level: '権限'
  };
  displayedHeaderColSpan: string[] = [
    'selection',
    'availability',
    'userID',
    'name',
    'organization',
    'post',
    'packageAuthority',
    'authority',
    'mailSendDivision',
    'comment',
    'lastLoginDT',
    'finalUpdate'
  ];
  displayedSubHeader: string[] = [
    'organizationID',
    'organizationNM',
    'factory',
    'caseFormApprovalStatus',
    'caseFormManager',
    'editLevel',
    'updateDT',
    'updateUserID'
  ];
  displayedColumns: string[] = [
    'selection',
    'availability',
    'userID',
    'name',
    'organizationID',
    'organizationNM',
    'factory',
    'post',
    'caseFormApprovalStatus',
    'caseFormManager',
    'editLevel',
    'mailSendDivision',
    'comment',
    'lastLoginDT',
    'updateDT',
    'updateUserID'
  ];
  /*    end of init variable     */

  constructor(
    private permissionService: PermissionService,
    private matDialog: MatDialog,
    private router: Router,
    private s3301Service: S3301Service
  ) {
    super();
    this.columnDisplay = {
      ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])
    };
    this.columnDisplayTmp = {
      ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])
    };
  }
  isMMC = false;
  userLoginEditLevel = 4;
  userLoginOrganizationID = '';
  ngOnInit(): void {
    this.userLogged = AuthService.currentUser;
    console.log(this.userLogged);
    this.isMMC = this.userLogged.userType === ROLE_CONST.MMC.LABEL;
    if (!this.isMMC) this.router.navigate([`dplog/${SCREEN_CONST.S0201.id}`]);
    this.processInit();
    this.userLoginOrganizationID = this.userLogged.userDepartment;
    this.searchUser();
    this.initDataSearch();
  }
  private resizeTable() {
    const columnResizeTmp: ResizeColumnModel[] = [
      { columnName: 'selection', width: 70 },
      { columnName: 'availability', width: 100 },
      { columnName: 'userID', width: 230 },
      { columnName: 'name', width: 200 },
      { columnName: 'organizationID', width: 180 },
      { columnName: 'organizationNM', width: 360 },
      { columnName: 'factory', width: 180 },
      { columnName: 'post', width: 150 },
      { columnName: 'caseFormApprovalStatus', width: 120 },
      { columnName: 'caseFormManager', width: 80 },
      { columnName: 'editLevel', width: 150 },
      { columnName: 'mailSendDivision', width: 110 },
      { columnName: 'comment', width: 200 },
      { columnName: 'lastLoginDT', width: 200 },
      { columnName: 'updateDT', width: 180 },
      { columnName: 'updateUserID', width: 150 }
    ];
    const columnResize: ResizeColumnModel[] = [];
    for (const i of columnResizeTmp) {
      if (!this.columnDisplay[i.columnName].hidden) {
        columnResize.push({ columnName: i.columnName, width: i.width });
      }
    }
    this.initTable('s3301-tb', columnResize);
  }
  ngAfterViewInit() {
    this.resizeTable();
    this.permissionService.initPermission(this.screen.functionId);
  }
  processInit() {
    this.searchModel.pageNumber = this.currentPage;
    this.searchModel.sortConditions = [
      {
        columnName: 'caseFormApprovalStatus',
        orderType: 'ASC'
      }
    ];
  }

  private initDataSearch() {
    this.initModel = new S3301InitModel();
    this.s3301Service.initDataSearch().subscribe((res: any) => {
      this.initModel = res.data?.content;
    });
  }
  private searchUser() {
    const search = new S3301SearchModel();
    search.userID = this.userLogged.userId;
    if (this.sortCols.length > 0) {
      search.sortConditions = [...this.sortCols];
    }

    const payload = { ...search };
    payload.displayItems = undefined;
    this.s3301Service.onSearchData(payload).subscribe((res: any) => {
      this.userLoginEditLevel = res.data.content[0].editLevel;
      console.log(this.userLoginEditLevel);
    });
    this.isEdit = false;
    this.isSelectedRow = false;
  }
  /*-------------------------------------------*/
  /*               HANDLE FUNCTION             */
  /*-------------------------------------------*/
  handleSearch(): void {
    this.searchErrorModel = new S3301SearchErrorModel();
    const valid = this.validationSearch();
    if (!valid) return;
    this.panelOpenResult = true;
    this.columnDisplay = structuredClone(this.columnDisplayTmp);
    this.resetInit(this.displayedColumns, this.columnDisplay);
    this.processColSpan();
    this.processSearch(true);
  }
  private updateOrganization() {
    this.s3301Model.organizationID = this.userLoginOrganizationID;
    console.log(this.s3301Model.organizationID);
    for (const i of this.initModel.organizations) {
      if (i.organizationID === this.s3301Model.organizationID) {
        this.s3301Model.organizationNM = i.organizationName;
        this.s3301Model.factory = i.factory;
        return;
      }
    }
  }
  handleEditRow(): void {
    if (this.userLoginEditLevel === 2) {
      this.updateOrganization();
    }
    this.isEdit = true;
    this.isAddNewMode = false;
    this.panelOpenSearch = false;
    this.panelOpenDisplayItem = false;
    const data = this.dataSource.data;
    const find = data.find(filter => filter.userID === this.s3301Model.userID);
    if (find) {
      find.edited = true;
      this.colSpans.organization = 3;
      this.colSpans.packageAuthority = 2;
      // Display full column
      this.showColsWhenEdit();
    }
  }
  handleSaveRow(): void {
    this.processConfirm(ACTION_TYPE_CONST.SAVE);
  }
  handleCancelRow(): void {
    this.processConfirm(ACTION_TYPE_CONST.CANCEL);
  }

  handleDeleteRow(): void {
    this.processConfirm(ACTION_TYPE_CONST.DELETE);
  }

  handleValidation(): void {
    this.s3301Service
      .onValidate({
        headers: {
          userID: this.s3301Model.userID,
          organizationID: this.s3301Model.organizationID
        }
      })
      .pipe(
        catchError(err =>
          CommonUtil.processHttpError(
            err,
            this.matDialog,
            MESSAGES_CODE.ES33004.Message
          )
        )
      )
      .subscribe({
        next: () => {
          DialogUtil.info(
            this.matDialog,
            getMessage(MESSAGES_CODE.WS33001.Code, [
              this.s3301Model.userID,
              this.s3301Model.organizationNM,
              this.getCaseFormApprovalStatus(
                this.s3301Model.caseFormApprovalStatus
              ),
              this.getEditLevel(this.s3301Model.editLevel)
            ])
          );
        }
      });
  }
  csvModel: S3301CSVModel = new S3301CSVModel();

  exeExportCsv(): void {
    this.searchModel.recordPerPage = PAGINATION_CONST.NUMBER_RECORD_FOR_SEARCH;
    this.searchModel.pageNumber = this.currentPage;

    if (this.sortCols?.length > 0) {
      this.searchModel.sortConditions = [...this.sortCols];
    }
    const fieldsTmp = [
      {
        label: '取引先コード',
        value: 'availability'
      },
      {
        label: 'ユーザーID',
        value: 'userID'
      },
      {
        label: '姓名',
        value: 'name'
      },
      {
        label: '組織ID',
        value: 'organizationID'
      },
      {
        label: '組織名称',
        value: 'organizationNM'
      },
      {
        label: '担当製作所',
        value: 'factory'
      },
      {
        label: '職務',
        value: 'post'
      },
      {
        label: '承認区分',
        value: 'caseFormApprovalStatus'
      },
      {
        label: '取纏め',
        value: 'caseFormManager'
      },
      {
        label: '編集権限レベル',
        value: 'editLevel'
      },
      {
        label: this.TRANS.mailSendDivision.JP,
        value: 'mailSendDivision'
      },
      {
        label: 'コメント',
        value: 'comment'
      },
      {
        label: '最終ログイン履歴日時',
        value: 'lastLoginDT'
      },
      {
        label: '更新日時',
        value: 'updateDT'
      },
      {
        label: '更新ユーザー',
        value: 'updateUserID'
      }
    ];
    const fields = [];
    for (const i of fieldsTmp) {
      if (this.columnDisplay[i.value].hidden === false) {
        fields.push({ label: i.label, value: i.value });
      }
    }
    const content = this.dataSource?.data;
    if (!content.length) {
      const message = getMessage(MESSAGES_CODE.EXPORT_404.Code);
      DialogUtil.error(this.matDialog, message);
      return;
    }
    // reload data
    const searchConditionsString = this.getSearchDataForExport();
    const displayItems = [].join(EXPORT_CONST.CSV.DELIMITER);
    const exportModel = new ExportModel(
      [],
      this.formatData(content),
      fields,
      searchConditionsString,
      displayItems
    );
    this.s3301Service.exportCsvWithNewTemplate(
      EXPORT_CONST.FILENAME.S33,
      exportModel
    );
  }
  handleCheckedChange(evt: MatRadioChange, model: S3301TableModel) {
    this.s3301Model = { ...model };
    this.oldUserID = this.s3301Model.userID;
    this.organizationID = this.s3301Model.organizationID;
    this.isSelectedRow = evt.source.checked;
  }
  handleAddRow() {
    this.isAddNewMode = true;
    this.isEdit = true;
    this.isSelectedRow = false;
    this.panelOpenSearch = false;
    this.panelOpenDisplayItem = false;
    this.s3301Model = new S3301TableModel();
    this.tableErrorModel = new S3301tableErrorModel();
    if (this.userLoginEditLevel === 2) {
      this.updateOrganization();
    }
  }
  /*end of handle function*/

  /* ----------------------------- */
  /*      some extend function     */
  /* ----------------------------- */
  private processSearch(resetColumn?: boolean) {
    this.s3301Model = new S3301TableModel();
    if (this.sortCols.length > 0) {
      this.searchModel.sortConditions = [...this.sortCols];
    }

    const payload = { ...this.searchModel };
    payload.displayItems = undefined;
    this.s3301Service
      .onSearchData(payload)
      .pipe(
        catchError(err =>
          CommonUtil.processHttpError(
            err,
            this.matDialog,
            MESSAGES_CODE.ES00002.Message
          )
        )
      )
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource<S3301TableModel>(
          res.data?.content
        );
        this.isActiveCSV = true;
        this.panelOpenSearch = false;
        this.panelOpenDisplayItem = false;
        if (resetColumn)
          setTimeout(() => {
            this.resetInit(this.displayedColumns, this.columnDisplay);
          }, 100);
      });
    this.isEdit = false;
    this.isSelectedRow = false;
  }

  private showColsWhenEdit() {
    Object.keys(this.columnDisplay).forEach((key: string) => {
      const property = this.columnDisplay[key];
      if (property) {
        this.columnDisplay[key].hidden = false;
      }
    });
  }
  displayItemChange(evt: DisplayItemConfigModel) {
    this.columnDisplayTmp = {
      ...CommonUtil.columnDisplay(
        this.fixedCols,
        this.displayedColumns,
        evt.columns
      )
    };
    this.pageSizeTmp = evt.itemPerPage * PAGINATION_CONST.RECORD_PER_PAGE;
    this.sortCols =
      evt.sorts.length > 0
        ? evt.sorts
        : [{ columnName: 'availability', orderType: SORT_TYPE.ASC }];

    if (evt.columns?.length > 0) {
      this.displayCols = [...evt.columns];
    }
  }
  private processConfirm(actionType: string) {
    let dialogMsg = '';
    const data = { actionType: actionType };

    if (actionType === ACTION_TYPE_CONST.SAVE) {
      dialogMsg = MESSAGES_CODE.W000001.Message;
    } else if (actionType === ACTION_TYPE_CONST.DELETE) {
      dialogMsg = MESSAGES_CODE.W000002.Message;
    } else if (actionType === ACTION_TYPE_CONST.CANCEL) {
      dialogMsg = MESSAGES_CODE.W000003.Message;
    }

    const valid = this.validationSaveItem();
    if (!valid && actionType == ACTION_TYPE_CONST.SAVE) {
      return;
    }
    const dialogRef = DialogUtil.confirm(this.matDialog, dialogMsg, data);
    this.tableErrorModel = new S3301tableErrorModel();
    dialogRef.afterClosed().subscribe(res => {
      if (res !== CONFIRM_DIALOG_CONTS.status.rejected) {
        this.processAction(res.data?.actionType);
      }
    });
  }
  private processAction(actionType: string) {
    if (actionType == ACTION_TYPE_CONST.SAVE) {
      this.processSave();
      return;
    }
    if (actionType === ACTION_TYPE_CONST.DELETE) {
      this.processDelete();
      return;
    }
    if (actionType === ACTION_TYPE_CONST.CANCEL) {
      this.processCancel();
    }
  }
  private processSave() {
    if (this.isAddNewMode) {
      this.callApiInsert();
    } else {
      this.callApiUpdate();
    }
  }
  private callApiInsert() {
    const insertModel: S3301InsertModel = {
      userID: this.s3301Model.userID,
      name: this.s3301Model.name,
      organizationID: this.s3301Model.organizationID,
      availability: this.s3301Model.availability,
      post: this.s3301Model.post,
      caseFormApprovalStatus: this.s3301Model.caseFormApprovalStatus,
      caseFormManager: this.s3301Model.caseFormManager,
      editLevel: this.s3301Model.editLevel,
      comment: this.s3301Model.comment,
      lastLoginDT: null,
      mailSendDivision: this.s3301Model.mailSendDivision,
      updateDT: null,
      selfUserID: null,
      selfOrganizationID: null
    };
    this.s3301Service
      .onInsertMMC(insertModel)
      .pipe(
        catchError((err: unknown) =>
          CommonUtil.processHttpError(
            err,
            this.matDialog,
            getErrorMessageResponse(err, null)
          )
        )
      )
      .subscribe(() => {
        this.resetVariable();
        DialogUtil.info(this.matDialog, MESSAGES_CODE.I000001.Message);
        this.processSearch();
      });
  }

  private callApiUpdate() {
    const updateModel: S3301UpdateModel = {
      userID: this.s3301Model.userID,
      name: this.s3301Model.name,
      organizationID: this.s3301Model.organizationID,
      availability: this.s3301Model.availability,
      post: this.s3301Model.post,
      caseFormApprovalStatus: this.s3301Model.caseFormApprovalStatus,
      caseFormManager: this.s3301Model.caseFormManager,
      editLevel: this.s3301Model.editLevel,
      comment: this.s3301Model.comment,
      lastLoginDT: this.s3301Model.lastLoginDT,
      mailSendDivision: this.s3301Model.mailSendDivision,
      updateDT: this.s3301Model.updateDT,
      selfUserID: this.oldUserID,
      selfOrganizationID: this.organizationID
    };
    this.s3301Service
      .onUpdateMMC(updateModel)
      .pipe(
        catchError((err: unknown) =>
          CommonUtil.processHttpError(
            err,
            this.matDialog,
            getErrorMessageResponse(err, null)
          )
        )
      )
      .subscribe(() => {
        this.resetVariable();
        DialogUtil.info(this.matDialog, MESSAGES_CODE.I000001.Message);
        this.processSearch();
      });
  }
  private resetVariable() {
    this.isEdit = false;
    this.isAddNewMode = false;
    this.isSelectedRow = false;
    for (const i of this.dataSource.data) {
      i.edited = false;
    }
    this.s3301Model = new S3301TableModel();
    this.tableErrorModel = new S3301tableErrorModel();
  }

  /*-----------------------------------*/
  /*        VALIDATION SAVE            */
  /*-----------------------------------*/
  private validationSaveItem(): boolean {
    if (this.s3301Model.availability === 2)
      this.s3301Model.mailSendDivision = 0;
    this.tableErrorModel = new S3301tableErrorModel();
    let valid = true;
    const value = this.s3301Model.userID?.trim();
    if (!value) {
      this.tableErrorModel.userID.isError = true;
      this.tableErrorModel.userID.msg = formatString(
        MESSAGES_CODE.C000001.Message,
        [this.nameJPItemSearch.UserID]
      );
      valid = false;
    } else if (value.length > 83) {
      this.tableErrorModel.userID.isError = true;
      this.tableErrorModel.userID.msg = formatString(
        MESSAGES_CODE.C000003.Message,
        [this.nameJPItemSearch.UserID, 83]
      );
      valid = false;
    } else if (!PATTERN_CONST.ALPHA_NUMERIC.test(value)) {
      this.tableErrorModel.userID.isError = true;
      this.tableErrorModel.userID.msg = formatString(
        MESSAGES_CODE.C000019.Message,
        [this.TRANS.userID.JP]
      );
      valid = false;
    }
    return this.validationSaveUserNMItem(valid);
  }
  private validationSaveUserNMItem(valid: boolean): boolean {
    const value = this.s3301Model.name?.trim();
    if (!value) {
      this.tableErrorModel.name.isError = true;
      this.tableErrorModel.name.msg = formatString(
        MESSAGES_CODE.C000001.Message,
        [this.TRANS.name.JP]
      );
      valid = false;
    } else if (value.length > 20) {
      this.tableErrorModel.name.isError = true;
      this.tableErrorModel.name.msg = formatString(
        MESSAGES_CODE.C000003.Message,
        [this.TRANS.name.JP, 20]
      );
      valid = false;
    }
    return this.validationSaveOrganizationID(valid);
  }
  private validationSaveOrganizationID(valid: boolean): boolean {
    const value = this.s3301Model.organizationID;
    if (!value) {
      this.tableErrorModel.organizationID.isError = true;
      this.tableErrorModel.organizationID.msg = formatString(
        MESSAGES_CODE.C000001.Message,
        [this.TRANS.organizationID.JP]
      );
      valid = false;
    } else if (
      this.s3301Model.editLevel === 2 &&
      this.s3301Model.organizationID !== this.userLoginOrganizationID
    ) {
      this.tableErrorModel.organizationID.isError = true;
      this.tableErrorModel.organizationID.msg = MESSAGES_CODE.ES33002.Message;
      valid = false;
    }
    return this.validationCaseFormApprovalStatus(valid);
  }
  private validationCaseFormApprovalStatus(valid: boolean): boolean {
    const value = this.s3301Model.caseFormApprovalStatus;
    if (this.s3301Model.editLevel < 4 && value !== 2) {
      this.tableErrorModel.caseFormApprovalStatus.isError = true;
      this.tableErrorModel.caseFormApprovalStatus.msg = formatString(
        MESSAGES_CODE.ES33001.Message,
        [
          this.TRANS.post.JP,
          this.TRANS.caseFormApprovalStatus.JP,
          '課長/MGまたは部長以上ではない',
          '承認'
        ]
      );
      valid = false;
    }
    return this.validationPosition(valid);
  }
  private validationPosition(valid: boolean): boolean {
    const value = this.s3301Model.post;
    if (value === 0) {
      this.tableErrorModel.post.isError = true;
      this.tableErrorModel.post.msg = formatString(
        MESSAGES_CODE.C000001.Message,
        [this.TRANS.post.JP]
      );
      valid = false;
    }
    return this.validationSaveEditLevel(valid);
  }

  private validationSaveEditLevel(valid: boolean): boolean {
    const value = this.s3301Model.editLevel;
    if (value === 0) {
      this.tableErrorModel.editLevel.isError = true;
      this.tableErrorModel.editLevel.msg = formatString(
        MESSAGES_CODE.C000001.Message,
        [this.TRANS.editLevel.JP]
      );
      valid = false;
    } else if (value === 4 && this.s3301Model.caseFormApprovalStatus === 1) {
      this.tableErrorModel.editLevel.isError = true;
      this.tableErrorModel.editLevel.msg = MESSAGES_CODE.ES33005.Message;
      valid = false;
    }
    return this.validationSaveComment(valid);
  }
  private validationSaveComment(valid: boolean): boolean {
    const value = this.s3301Model.comment?.trim();
    if (value?.length > 100) {
      this.tableErrorModel.editLevel.isError = true;
      this.tableErrorModel.editLevel.msg = formatString(
        MESSAGES_CODE.C000003.Message,
        [this.TRANS.comment.JP, 100]
      );
      valid = false;
    }
    return valid;
  }
  private validationSearch() {
    let valid = true;
    const value = this.searchModel.userID?.trim();
    if (value && value.length > 100) {
      this.searchErrorModel.userID.isError = true;
      this.searchErrorModel.userID.msg = formatString(
        MESSAGES_CODE.C000003.Message,
        [this.TRANS.userID.JP, 100]
      );
      valid = false;
    }
    return valid;
  }
  /*     end of VALIDATION SAVE        */

  /*----------------------------------------*/
  /*           child function              */
  /*----------------------------------------*/
  private processColSpan() {
    this.colSpans = { organization: 0, packageAuthority: 0, finalUpdate: 0 };

    if (!this.columnDisplay['organizationID'].hidden) {
      this.colSpans.organization += 1;
    }
    if (!this.columnDisplay['organizationNM'].hidden) {
      this.colSpans.organization += 1;
    }
    if (!this.columnDisplay['factory'].hidden) {
      this.colSpans.organization += 1;
    }
    if (!this.columnDisplay['caseFormApprovalStatus'].hidden) {
      this.colSpans.packageAuthority += 1;
    }
    if (!this.columnDisplay['caseFormManager'].hidden) {
      this.colSpans.packageAuthority += 1;
    }
    if (!this.columnDisplay['updateDT'].hidden) {
      this.colSpans.finalUpdate += 1;
    }
    if (!this.columnDisplay['updateUserID'].hidden) {
      this.colSpans.finalUpdate += 1;
    }
  }
  private processDelete() {
    const payload = {
      body: {
        userID: this.s3301Model.userID,
        organizationID: this.s3301Model.organizationID
      }
    };
    this.s3301Service
      .onDeleteMMC(payload)
      .pipe(
        catchError((err: unknown) =>
          CommonUtil.processHttpError(
            err,
            this.matDialog,
            getErrorMessageResponse(err, null)
          )
        )
      )
      .subscribe((res: any) => {
        const responseMsg = res.message;
        if (res.status === HTTP_STATUS.Ok && !responseMsg) {
          DialogUtil.info(this.matDialog, MESSAGES_CODE.I000002.Message);
        } else {
          DialogUtil.error(this.matDialog, MESSAGES_CODE[responseMsg]?.Message);
        }
        this.resetVariable();
        this.processSearch();
      });
  }
  private processCancel() {
    this.resetVariable();
  }
  private getSearchDataForExport() {
    const searchConditions = [
      [
        this.TRANS.availability.JP,
        this.TRANS.userID.JP,
        this.TRANS.organizationID.JP,
        this.TRANS.post.JP
      ].join(EXPORT_CONST.CSV.DELIMITER),
      [
        convertTextCsv(
          this.searchModel.availability === null
            ? ''
            : this.getAvailable(this.searchModel.availability)
        ),
        convertTextCsv(this.searchModel.userID),
        convertTextCsv(this.searchModel.organizationID),
        convertTextCsv(
          this.searchModel.level === null
            ? ''
            : this.getEditLevel(this.searchModel.level)
        )
      ].join(EXPORT_CONST.CSV.DELIMITER),
      CHAR_ITEM.BLANK //add more one end of line in csv
    ].join(EXPORT_CONST.EOL);
    return searchConditions;
  }
  private formatData(data: S3301TableModel[]): unknown[] {
    const dataReturn = [];
    for (const item of data) {
      dataReturn.push({
        availability: this.getAvailable(item.availability),
        userID: item.userID,
        name: item.name,
        organizationID: item.organizationID,
        organizationNM: item.organizationNM,
        factory: item.factory,
        post: this.getPosition(item.post),
        caseFormApprovalStatus: this.getCaseFormApprovalStatus(
          item.caseFormApprovalStatus
        ),
        caseFormManager: this.getCollect(item.caseFormManager),
        editLevel: this.getEditLevel(item.editLevel),
        mailSendDivision: this.getMailNotification(item.mailSendDivision),
        comment: item.comment,
        lastLoginDT: item.lastLoginDT,
        updateDT: item.updateDT
      });
    }
    return dataReturn;
  }
  private getAvailable(key: number) {
    return this.availability[key].label;
  }
  private getPosition(key: number) {
    return key === 0
      ? ''
      : key === 1
      ? '作業長/副作業長'
      : key === 2
      ? '係長/主任'
      : key === 3
      ? 'シニアスタッフ'
      : key === 4
      ? '課長'
      : '部長以上';
  }
  private getCaseFormApprovalStatus(key) {
    return key === 0 ? '' : key === 1 ? '点検' : '承認';
  }
  private getCollect(key) {
    return key === 0 ? '' : '〇';
  }
  private getEditLevel(key) {
    return key === 0
      ? '無し'
      : key === 1
      ? '一般'
      : key === 2
      ? '業務取纏担当'
      : key === 3
      ? '調達取引先担当'
      : '運用担当者';
  }
  private getMailNotification(key) {
    return key === 1 ? '有効' : '無効';
  }
  onChangeOrganizeID(event) {
    for (const i of this.initModel.organizations) {
      if (i.organizationID === event.value) {
        this.s3301Model.organizationNM = i.organizationName;
        this.s3301Model.factory = i.factory;
        return;
      }
    }
  }
}
