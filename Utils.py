from constant import *
from readCSV import *

class Util:
    def __init__(self, name, lvl: int, csvReader: CSVReader, row: RowObj):
        self.name = name
        self.lvl = lvl
        self.data = csvReader
        self.row = row

class Element:
    def __init__(): pass

class Element:
    def __init__(self, o: Util, params: list, childs: Element):
        self.o = o
        self.params = params
        self.childs = childs
        self.nameTag = 'element'
        self.lvl = o.lvl
        self.ctx = ""
        self.visit()
        
    def hiddenText(self,name):
        return '[hidden]="' + "columnDisplay['{}'".format(name)+'].hidden"'
    
    def visit(self):
        pass

    def wl(self, msg, lvl):
        self.ctx += f'{getSpace(lvl)}{msg}\n'

    def w(self, msg, lvl):
        self.ctx += msg

    def wc(self, msg, lvl):
        self.ctx += f'{getSpace(lvl)}{msg}'

    def getCTX(self):
        if len(self.params) > 0:
            self.wl('<{}'.format(self.nameTag), self.lvl)
            for param in self.params:
                self.wl(param, self.lvl+1)
            self.wl('>', self.lvl)
        else:
            self.wl('<{}>'.format(self.nameTag), self.lvl)
        for child in self.childs:
            self.w(child.getCTX(), self.lvl)
        self.wl('</{}>'.format(self.nameTag), self.lvl)
        return self.ctx

class Span(Element):
    def visit(self):
        self.nameTag='span'
        self.content = self.o.row.jp
    def getCTX(self):
        if len(self.params) > 0:
            self.wl('<{}'.format(self.nameTag), self.lvl)
            for param in self.params:
                self.wl(param, self.lvl+1)
            self.wl('>{}</{}>'.format(self.content,self.nameTag), self.lvl)
        else:
            self.wl('<{}>{}</{}>'.format(self.nameTag,self.content,self.nameTag), self.lvl)
        return self.ctx
class Div(Element):
    def visit(self):
        self.nameTag = 'div'
class ButtonDelete(Element):
    def __init__(self, lvl:int,name:str,model:str,params):
        self.lvl = lvl
        self.name = name
        self.nameTag = 'button'
        self.params = [
            'matSuffix',
            'mat-icon-button',
            'aria-label="Clear"',
            'class="cm-icon cm-icon-clear"',
            '(click)="{}.{} = '.format(model,name)+ "''" + '"' ]+params
        self.ctx= ""
        self.childs = [Text(getSpace(lvl + 1)+'<mat-icon>close</mat-icon>\n')]
class MatRadioButton(Element):
    def __init__(self, lvl:int):
        self.lvl = lvl
        self.nameTag = 'mat-radio-button'
        self.params = [
            'class="radio-button-normal"',
            'color="primary"',
            '[disabled]="isEdit || isAddNewMode"',
            '(change)="handleCheckedChange($event, element)"']
        self.ctx= ""
        self.childs = []
        
class Input(Element):
    def __init__(self,lvl:int, name:str, model:str, params:list):
        self.ctx = ""
        self.lvl = lvl
        self.name = name
        self.model = '[(ngModel)]="{}"'.format(model)
        self.params =['matInput',
                      'aria-label="{}"'.format(name),
                      'id="{}"'.format(name),
                      'name="{}"'.format(name)] + [self.model] + params
    def getCTX(self):
        self.wl('<input', self.lvl)
        for param in self.params:
            self.wl(param, self.lvl+1)
        self.wl('/>', self.lvl)
        return self.ctx
    
class Text(Element):
    def __init__(self,content):
        self.content = content
    def getCTX(self):
        return self.content
class MatOption(Element):
    def __init__(self,lvl, value, label):
        self.ctx = ""
        self.lvl = lvl
        self.nameTag = 'mat-option'
        if type(value) is int:
            self.params = ['[value]="{}"'.format(value)]
        else:
            self.params = ['[value]="{}"'.format("'" + value + "'")]
        self.childs = [Text(label)]
    def getCTX(self):
        if len(self.params) > 0:
            self.wc('<{}'.format(self.nameTag), self.lvl)
            for param in self.params:
                self.w(' '+param, self.lvl+1)
            self.w('>', self.lvl)
        else:
            self.wc('<{}>'.format(self.nameTag), self.lvl)
        for child in self.childs:
            self.w(child.getCTX(), self.lvl)
        self.w('</{}>'.format(self.nameTag), self.lvl)
        self.ctx+='\n'
        return self.ctx
class MatSelect(Element):
    def __init__(self,lvl:int, model, params:list, options:list):
        self.ctx = ""
        self.nameTag = 'mat-select'
        self.lvl = lvl
        param = '[(ngModel)]="editModel.{}"'.format(model)
        self.params = [param] + params
        # create childs from option ['','']
        childs = []
        for option in options:
            childs += [MatOption( lvl + 1,option[0],option[1])]
        self.childs = childs 
class MatFormFiled(Element):
    def visit(self):
        self.nameTag = 'mat-form-field'
        self.params = ['appearance="outline"','[ngClass]="' + "{'mat-form-field-invalid': tableErrorModel."+ self.o.row.name + '.isError}"' ]
        
class TableRow(Element):
    def visit(self):
        self.nameTag = 'tr'


class TableFooter(Element):

    def visit(self):
        self.nameTag = 'td'
        self.params += ['mat-footer-cell', '*matFooterCellDef']
        if self.o.row.type != 'select':
            self.params += [self.hiddenText(self.o.row.name)]


class TableBody(Element):

    def visit(self):
        self.nameTag = 'td'
        self.params += ['mat-cell', '*matCellDef="let element"']
        if self.o.row.type == 'select':
            self.params += ['class="selection"']
        if self.o.row.type != 'select':
            self.params += [self.hiddenText(self.o.row.name)]


class TableHead(Element):
    def visit(self):
        self.nameTag = 'th'
        param = ['mat-header-cell','*matHeaderCellDef']
        if self.o.row.rowSpan == 2:
            param += ['rowspan="2"']
        if self.o.row.colSpan > 1:
            param += ['colspan="colSpans.{}"'.format(self.o.row.name)]
        if self.o.row.type != 'select':
            if self.o.row.type == TYPE_LABEL:
                cond = '[hidden]="'
                for row in self.o.row.subHeader:
                    cond += "columnDisplay['{}'].hidden && ".format(row)  
                cond = cond[:-4]
                cond+='"'
                param += [cond]
            else :
                param += [self.hiddenText(self.o.row.name)]
        else:
            param += ['class="selection"']
        self.params = param + self.params
        o = self.o
        o.lvl += 1
        self.childs = [Span(o,[],[])]

class NgContainer(Element):
    def visit(self):
        self.nameTag = 'ng-container'
        columnName = self.o.row.name
        self.params = ['matColumnDef="{}"'.format(columnName)]


class MatTable(Element):
    def visit(self):
        self.nameTag = 'table'
        param = ['tableResize',
                 'mat-table',
                 '[dataSource]="dataSource"',
                 'matSort',
                 'aria-describedby="tableResult"',
                 'id="{}-tb"'.format(self.o.data.id),
                 'class="cm-table-result mat-table"']
        self.params = param + self.params
        util = self.o
        util.lvl += 1
        self.childs += [TableRow(util, ['mat-header-row', '*matHeaderRowDef="displayedHeaderColSpan"'], []),
                        TableRow(util, ['mat-header-row', '*matHeaderRowDef="displayedSubHeader"'], []),
                        TableRow(util, ['mat-row', '*matRowDef="let row; columns: displayedColumns"', 'class="{{ row.selected }}"'], [])]
        if self.o.data.footer:
            self.childs += [TableRow(util, ['mat-footer-row','*matFooterRowDef="displayedColumns"'], [])]
