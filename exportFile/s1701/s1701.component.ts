import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

import {
  ACTION_TYPE_CONST,
  BUTTON,
  CONFIRM_DIALOG_CONTS,
  EXPORT_CONST,
  FORMAT_DATE_CONST,
  HTTP_STATUS,
  PAGINATION_CONST,
  PATTERN_CONST,
  ROLE_CONST,
  SCREEN_CONST,
  SORT_TYPE,
} from 'src/app/shared/constants/common.constant';

import { MESSAGES_CODE } from 'src/app/shared/constants/messages.constant';
import { PERMISSION_ACTION_CONST } from 'src/app/shared/constants/permission.constant';
import { TableHandle } from 'src/app/shared/handles/table-v1.handle';
import { ColumnDataModel } from 'src/app/shared/models/display-item/column-data.model';
import { DisplayItemConfigModel } from 'src/app/shared/models/display-item/display-item-config.model';
import { SortDataModel } from 'src/app/shared/models/display-item/sort-data.model';
import { ExportModel } from 'src/app/shared/models/export.model';
import { ResizeColumnModel } from 'src/app/shared/models/resize-column.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { S1701Service } from 'src/app/shared/services/s1701.service';
import { CommonUtil } from 'src/app/shared/utils/common.util';
import { DialogUtil } from 'src/app/shared/utils/dialog.util';
import { getErrorMessageResponse, getMessage } from 'src/app/shared/utils/message.util';
import { convertTextCsv, formatString } from 'src/app/shared/utils/string.util';

@Component({ selector: 'app-S1701',templateUrl: './S1701.component.html',styleUrls: ['./S1701.component.scss']})
export class S1701Component
  extends TableHandle
  implements OnInit, AfterViewInit
{

  screen = SCREEN_CONST.S1701;
  button = BUTTON;
  isEdit = false;
  permissionActionConst = PERMISSION_ACTION_CONST;
  message = '';
  panelOpenSearch = true;
  panelOpenDisplayItem = true;
  panelOpenResult = false;
  isAddNewMode = false;

  columnDisplay = {};
  columnDisplayTmp = {};
  sortCols: SortDataModel[] = [];
  fixedCols = ['selection'];
  searchModel = new S1701SearchModel();
  tableErrorModel = new S1701tableErrorModel();
  editModel = new S1701TableModel();
  dataSource: MatTableDataSource<S1701TableModel> = new MatTableDataSource<S1701TableModel>();

/* item screen */
  ITEM_SCREEN = {
    selection : { columnName : 'selection', JP : '選択' },
    supplierCD : { columnName : 'supplierCD', JP : '取引先コード' },
    factory : { columnName : 'factory', JP : '納製' },
    partsNO : { columnName : 'partsNO', JP : '部品番号' },
    partsNM : { columnName : 'partsNM', JP : '部品名称' },
    receptionCD : { columnName : 'receptionCD', JP : '受入拠点コード' },
    receptionName : { columnName : 'receptionName', JP : '受入拠点名称' },
    conversionStartDay : { columnName : 'conversionStartDay', JP : '適用開始日' },
    shipingCD : { columnName : 'shipingCD', JP : '出荷拠点コード' },
    shipingNMSub : { columnName : 'shipingNMSub', JP : '出荷拠点名称' },
    approvalStatus : { columnName : 'approvalStatus', JP : '承認区分' },
    status : { columnName : 'status', JP : '利用区分' },
    column1 : { columnName : 'column1', JP : 'haha' },
    coment : { columnName : 'coment', JP : 'コメント' },
    approvalUserComent : { columnName : 'approvalUserComent', JP : '承認者コメント' },
  };

/* Item SUPPLIERCD */
  SUPPLIERCD_OPTION = [
    { value : 0, label : 'haha' },
    { value : 1, label : 'hihi' },
  ]

  colSpans = { column1: 2 };
  /* Create displayed Constant */
  displayedHeaderColSpan: string[] = [
    'selection',
    'supplierCD',
    'factory',
    'partsNO',
    'partsNM',
    'receptionCD',
    'receptionName',
    'conversionStartDay',
    'shipingCD',
    'shipingNMSub',
    'approvalStatus',
    'status',
    'column1'
  ];
  displayedSubHeader: string[] = [
    'coment',
    'approvalUserComent'
  ];
  displayedColumns: string[] = [
    'selection',
    'supplierCD',
    'factory',
    'partsNO',
    'partsNM',
    'receptionCD',
    'receptionName',
    'conversionStartDay',
    'shipingCD',
    'shipingNMSub',
    'approvalStatus',
    'status',
    'coment',
    'approvalUserComent'
  ];

  constructor(
    private permissionService: PermissionService,
    private matDialog: MatDialog,
    private router: Router,
    private s1701Service: S1701Service
  ) {
    super();
    this.columnDisplay = {
    ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])
    };
    this.columnDisplayTmp = {
    ...CommonUtil.columnDisplay(this.fixedCols, this.displayedColumns, [])
    };
  };

  ngOnInit(): void {
    this.processInit();
  };

  ngAfterViewInit() {
    this.resizeTable();
    this.permissionService.initPermission(this.screen.functionId);
  }

  private resizeTable() {
    const columnResizeTmp: ResizeColumnModel[] = [
      { columnName: 'selection', width: 100 },
      { columnName: 'supplierCD', width: 100 },
      { columnName: 'factory', width: 100 },
      { columnName: 'partsNO', width: 100 },
      { columnName: 'partsNM', width: 100 },
      { columnName: 'receptionCD', width: 100 },
      { columnName: 'receptionName', width: 100 },
      { columnName: 'conversionStartDay', width: 100 },
      { columnName: 'shipingCD', width: 100 },
      { columnName: 'shipingNMSub', width: 100 },
      { columnName: 'approvalStatus', width: 100 },
      { columnName: 'status', width: 100 },
      { columnName: 'coment', width: 100 },
      { columnName: 'approvalUserComent', width: 100 }
    ];
    const columnResize: ResizeColumnModel[] = [];
    for (const i of columnResizeTmp) {
      if (!this.columnDisplay[i.columnName].hidden) {
        columnResize.push({ columnName: i.columnName, width: i.width });
      }
    }
    this.initTable('s1701-tb', columnResize);
  }

  processInit() {
    this.searchModel.pageNumber = 1;
    this.searchModel.sortConditions = []
  }
}