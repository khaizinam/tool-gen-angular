import { CommonRequestModel } from '../common-request.model';
import { ErrorControlModel } from '../error-control.model';

/* Model */

/* Table Model */
export class S1701TableModel {
  supplierCD: number;
  factory: string;
  partsNO: string;
  partsNM: string;
  receptionCD: string;
  receptionName: string;
  conversionStartDay: string;
  shipingCD: string;
  shipingNMSub: string;
  approvalStatus: string;
  status: string;
  coment: string;
  approvalUserComent: string;
  edited: boolean;
  constructor() {
    this.supplierCD = 0;
    this.factory = '';
    this.partsNO = '';
    this.partsNM = '';
    this.receptionCD = '';
    this.receptionName = '';
    this.conversionStartDay = '';
    this.shipingCD = '';
    this.shipingNMSub = '';
    this.approvalStatus = '';
    this.status = '';
    this.coment = '';
    this.approvalUserComent = '';
    this.edited = false;
  }
}
/* Update Model */
export class S1701UpdateModel {
  supplierCD: number;
  partsNO: string;
  partsNM: string;
  receptionCD: string;
  receptionName: string;
  conversionStartDay: string;
  shipingCD: string;
  shipingNMSub: string;
  approvalStatus: string;
  status: string;
  coment: string;
  approvalUserComent: string;
}
/* Insert Model */
export class S1701InsertModel {
  supplierCD: number;
  partsNO: string;
  receptionCD: string;
  shipingNMSub: string;
}
/* Error Model */
export class S1701tableErrorModel {
  selection: ErrorControlModel = new ErrorControlModel();
  supplierCD: ErrorControlModel = new ErrorControlModel();
  partsNO: ErrorControlModel = new ErrorControlModel();
  partsNM: ErrorControlModel = new ErrorControlModel();
  receptionCD: ErrorControlModel = new ErrorControlModel();
  receptionName: ErrorControlModel = new ErrorControlModel();
  conversionStartDay: ErrorControlModel = new ErrorControlModel();
  shipingCD: ErrorControlModel = new ErrorControlModel();
  shipingNMSub: ErrorControlModel = new ErrorControlModel();
  approvalStatus: ErrorControlModel = new ErrorControlModel();
  status: ErrorControlModel = new ErrorControlModel();
  coment: ErrorControlModel = new ErrorControlModel();
  approvalUserComent: ErrorControlModel = new ErrorControlModel();
}
