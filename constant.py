fRoot = "s0901/"
fName = "s0901-component.html.ts"
TYPE_TEXT = "text"
TYPE_COMBO = "combo"
TYPE_SELECT = "select"
TYPE_LABEL = 'label'
def getSpace(level):
        s = ""
        for i in range(level):
            s += "  "
        return s

def toUpper(s:str):
    s1 = ""
    for i in s:
        if i.isupper():
            s1+=f"_{i.upper()}"
        else:
            s1+=f"{i.upper()}"
    return s1
def wLine( s, lvl):
    return f'{getSpace(lvl)}{s}\n'
    
def line( s, lvl):
    return f'{getSpace(lvl)}{s}'
